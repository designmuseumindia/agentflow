import React from 'react';
import ReactDOM from 'react-dom';
import DemoBar from './demobar';
// eslint-disable-next-line no-unused-vars
import FormBuilder, { Registry } from './src/agentflow';
import * as variables from './variables';
import APPP from './src/App'
import TREE from './src/components/agentflow/Tree'
require('./scss/application.scss');

import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  useParams,
} from 'react-router-dom';
 
import Home from './src/Home';
import About from './src/About';
import Login from './src/Login';
import NotFound from './src/NotFound';
import restricted from './src/Restricted';
import { Menu } from 'antd'
import Card from 'antd/lib/card/Card'
import { Drawer, Layout,Col, Row ,Image,Button} from 'antd'
import Navbar from './src/components/Navbar';
import './src/function';
// Add our stylesheets for the demo.

 
 
const url = 'http://127.0.0.1:5005/api/formdata';
const saveUrl = 'http://127.0.0.1:5005/api/formdata';

const { Header, Footer, Sider, Content } = Layout;


const Tools = {
  0: {
    title: 'Input to text',
    url: '/',
  },
  1: {
    title: 'Input to textarea',
    url: '/',
  },
  2: {
    title: 'Input to button',
    url: '/',
  },
  3: {
    title: 'Input to label',
    url: '/',
  },
};
const test = () => {
  <div>
  <h3>
    You're lost. Go to <Link to='/ranking'>Ranking</Link>
  </h3>
</div>

}
const Toolbar = () => (
  <>
   
<br/>
    <Routes>
      <Route exact path="/" element={<ToolbarList />} />
      <Route path="/:id" element={<ToolbarItem />} />
    </Routes>
  </>
);

const ToolbarList = () => (
  <>
    <h2>All Toolbar</h2>
    <ul>
      {Object.keys(Tools).map(key => (
        <li key={key}>
          Go to  toolbar item :&nbsp;
          <Link to={`/update/${key}`}>{Tools[key].title}</Link>
        </li>
      ))}
    </ul>
  </>
);

const ToolbarItem = () => {
  const { id } = useParams();

  return (
    <>
      <h2>{Tools[id].title}</h2>
      <p>
        Go to <a href={Tools[id].url}>Toolbar</a>
      </p>
      <p>
        Back to <Link to="/update">Toolbar</Link>
      </p>
    </>
  );
};

const Contents = () => (
<Routes>
  {/* Dashboard page */}
<Route exact path="/home" element={<Home />} />


<Route exact path="/" element={<Home />} />


{/* Toolbar page */}
<Route path="/update/*" element={<Toolbar />} />
<Route path="/about" element={<About />} />
<Route exact path="/" component={ test }/>
<Route exact path="/" element={<Home />} />
<Route path="/about" element={<About />} />
<Route path="/agentflow" element={<VisualEditor />} />


<Route exact path="/login" component={Login} />

<Route path='*' exact={true} element={<NotFound />}   />
</Routes>
);

const Navigation = () => (
 


<Menu className='sider'>
        <ul class="list-group list-group-flush ">
          <li className='list-group-item'>

          <Link to="/"> 
              <Card className='card'>
                <div className='content d-flex justify-content-between align-items-center'>
                  <div className='names '>Dashbord</div>
                  <div className='img-icons '><img src="/images/Vector.png" className='img-fluid img-thumbnail ' style={{width: "40px"}}></img></div>
                </div>
              </Card>
              </Link>

            </li>
            <li className='list-group-item'>
            <Link to="/about">  <Card className='card'>
                <div className='content d-flex justify-content-between align-items-center'>
                  <div className='names'>Firm Partners</div>
                  <div className='img-icons '><img src="/images/Vector (1).png" className='img-fluid img-thumbnail' style={{width: "40px"}}></img></div>
                </div>
              </Card></Link>
            </li>
            <li className='list-group-item'>
            <Link to="/update">  <Card className='card'>
                <div className='content d-flex justify-content-between align-items-center'>
                  <div className='names  '>Seminars</div>
                  <div className='img-icons  '><img src="/images/seminar 1.png" className='img-fluid img-thumbnail' style={{width: "40px"}}></img></div>
                </div>
              </Card></Link>
            </li>
            <li className='list-group-item'>
            <Link to="/agentflow">   <Card className='card'>
                <div className='content d-flex justify-content-between align-items-center'>
                  <div className='names  '>AgentFlow</div>
                  <div className='img-icons  '><img src="/images/clarity_users-line.png" className='img-fluid img-thumbnail' style={{width: "40px"}}></img></div>
                </div>
              </Card></Link>
            </li>
            <li className='list-group-item'>
            <Link to="/NotFound">  <Card className='card'>
                <div className='content d-flex justify-content-between align-items-center'>
                  <div className='names  '>AORG Certified professionals</div>
                  <div className='img-icons '><img src="/images/man.png" className='img-fluid img-thumbnail' style={{width: "40px"}}></img></div>
                </div>
              </Card></Link>
            </li>
            <li className='list-group-item'>
            <Link to="/NotFound">  <Card className='card'>
                <div className='content d-flex justify-content-between align-items-center'>
                  <div className='names '>Raw Data Uplode</div>
                  <div className='img-icons'><img src="/images/45 Upload Files.png" className='img-fluid img-thumbnail' style={{width: "40px"}}></img></div>
                </div>
              </Card></Link>
            </li>
            <li className='list-group-item'>
            <Link to="/NotFound">  <Card className='card'>
                <div className='content d-flex justify-content-between align-items-center'>
                  <div className='names'>Meta Data</div>
                  <div className='img-icons'><img src="/images/Vector (2).png" className='img-fluid img-thumbnail' style={{width: "40px"}}></img></div>
                </div>
              </Card>
              </Link>

            </li>
          </ul>
      </Menu>





);











  const TestComponent = () => <h2>Hello</h2>;

  const MyInput = React.forwardRef((props, ref) => {
    const { name, defaultValue, disabled } = props;
    return (
      <>
       <label style={{ marginRight: '1rem' }}><b>{ props.data.label }</b></label>
        <input ref={ref} name={name} defaultValue={defaultValue} disabled={disabled} />;
      </>
    );
  });

  Registry.register('MyInput', MyInput);
  Registry.register('TestComponent', TestComponent);

  const items = [{
      key: 'Header',
    }, {
      key: 'TextInput',
   }, {
     key: 'TextArea',
    }, {
      key: 'RadioButtons',
    }, {
      key: 'Checkboxes',
    }, {
       key: 'Image',
    },
    {
      key: 'TwoColumnRow'
    },
    {
      key: 'ThreeColumnRow'
    },
    {
      key: 'FourColumnRow'
    },
    {
      key: 'TestComponent',
      element: 'CustomElement',
      component: TestComponent,
      type: 'custom',
      field_name: 'test_component',
      name: 'Something You Want',
      icon: 'fa fa-cog',
      static: true,
      props: { test: 'test_comp' },
      label: 'Label Test',
    },
    {
      key: 'Button',
      element: 'CustomElement',
      component: MyInput,
      type: 'custom',
      forwardRef: true,
      bare: true,
      field_name: 'my_input_',
      name: 'My Input',
      icon: 'fa fa-cog',
      props: { test: 'test_input' },
      label: 'Label Input',
    },
  ];



  const handleClick = (e) =>{
    //this.setState({name: e.target.value});

  

 

      var formjsondata=JSON.stringify(this.state.data);
      
    
  
      alert('A name was submitted: ' + formjsondata);
     
 


    const getData   = $('.build-wrap').formBuilder('getData');

    
    //var newdata=sessionStorage.getItem('data');
 
    var role=window.sessionStorage.getItem('roles')

  /*
    
    var body = {
      id:null,
      parentid: role,
      name: 'Screen build-',
      screen_code:JSON.stringify(getData) 
  }

    axios({
      method: "post",
      url: "https://chartersasia.com/agentflow/rest/emp/addscreen",
      data: body,
       
        headers: {
      
          'Content-Type': "multipart/form-data"

        },




    })
      .then(function (response) {
        //handle success
        alert('Screen saved '+response.status)
        console.log(response);
      })
      .catch(function (response) {
        //handle error
        alert('Screen failed to save '+response.status)


        console.log(response);
      });




*/



  }









 
const VisualEditor = () => (


<div> 

<DemoBar variables={variables} />

<FormBuilder.ReactFormBuilder
    variables={variables}
	 
    url={url}
    saveUrl={saveUrl}
    locale='en'
    saveAlways={true}
    // toolbarItems={items}
  />


<Layout style={{ float:'right',background: '#fff', width:'75%',textAlign: 'center', padding: 0 }} >

<div className='screenadd' style={{color:'#820024',fontWeight:'bold',fontSize:'14px'}} >
<a type="primary" className='screenbtn' style={{color:'#820024',fontWeight:'bold',fontSize:'24px',width:'100px'}}    class="card-footer dndfooter" onClick={handleClick}    id="createscreen" block >
Add Screen
    </a> 
    </div>



          <center> <TREE/></center>
           </Layout>

</div>
 
);


const App = () => (


<div> 
      {/*<AddEditSubtenant/>
  <AddEditCoordinator/>*/}
     
     <Router>




     <Layout>
        

        <Sider breakpoint='lg'
                className='border-0 border-right'
                theme='light'
                width={300}
                collapsedWidth={0}
                trigger={null}
                >


<div class="">
        <img src='images/logoImage.svg' className='logo' />
      </div>


                  <Navigation />
                </Sider>


        
        <Layout>
          <Header style={{ background: '#fff', textAlign: 'center', padding: 0 }} >

<Row>
<Col span={18} > 
<Navbar className="">
<Navigation />
</Navbar>
</Col>
<Col span={6}> 
<div className="d-flex align-items-center ">
<Image src='/images/bell.png' className='bell d-none d-md-block d-lg-block' width={20} />
<p className='text d-none d-md-block d-lg-block m-0 '>Hi, Super Admin</p>
<Image className='admin' src='/images/spouse1.png' width={50}/>
</div></Col>

</Row>



          </Header>
          <Content style={{ margin: '24px 16px 0' }}> <Contents  className='layout-container'  />
          

          
          
          </Content>
          <Footer>  Agentflow</Footer>
        </Layout>
      </Layout>













    




 


 


        </Router>
  
    
  </div>






);

ReactDOM.render(
  <App />,
  document.getElementById('form-builder'),
);

 